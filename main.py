# zad 5
# def isEven(x):
#     if x % 2 == 0:
#         print("Liczba "+str(x)+" jest parzysta")
#     else:
#         print("Liczba jest nieparzysta")
#
# a = int(input("Podaj liczbę: "))
# isEven(a)

# zad 5.1

# import random
# def arrayEven(tab):
#     for a in tab:
#         if a % 2 == 0:
#             print(str(a)+" jest parzyste")
#         else:
#             print(str(a)+" jest nieparzyste")
#
# def createList():
#     tab = list()
#     for i in range(0, 10):
#         tab.append(int(random.randrange(1, 100)))
#     return tab
#
# arrayEven(createList())

# zad 6
# def basicActions(a, b, c):
#     return eval(b+a+c)
#
# def menu(response=0, another=0):
#     if another:
#         print("Aby zakończyć działanie programu wciśnij enter.")
#     a = ""
#     b = 0
#     c = 0
#     signs = ["+", "-", "*", "/", "pow"]
#     if response == 1:
#         a = input("Wybierz podane działania lub podaj prawidłową liczbę: +, -, *, /, pow: ")
#     elif response == 0:
#         a = input("Podaj działanie +, -, *, / lub pow: ")
#     if a == "":
#         print("Nie podano wartości. Zakończono działanie programu.")
#         return
#     if a not in signs:
#         print("Podano błędne działanie.")
#         menu(1)
#     try:
#         b = int(input("Podaj pierwszą liczbę: "))
#         c = int(input("Podaj drugą liczbę: "))
#     except:
#         print("Podano nieprawidłową liczbę.")
#         menu(1)
#
#     if a != "pow":
#         print(basicActions(str(a), str(b), str(c)))
#         menu(0, 1)
#     else:
#         print(pow(b, c))
#         menu(0, 1)
#
# menu()

# zad 8
# def primeNumbers(x):
#     divisors = list()
#     for i in range(2, x):
#         if x % i == 0:
#             divisors.append(i)
#     try:
#         if divisors[0]:
#             print("Liczba "+str(x)+" nie jest pierwsza")
#     except:
#         print("Liczba "+str(x)+" jest pierwsza")
#     return
#
# def rangePrime(x):
#     for i in range(2, x):
#         primeNumbers(i)
#     return
#
# a = int(input("Podaj liczbę: "))
# rangePrime(a)
