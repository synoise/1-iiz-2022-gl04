def znajdzSilnie(x):
    i = 1
    y = 1
    while (i <= x):
        y = y * i
        i = i + 1
    return y


# v = int(input("Podaj liczbę: "))
# z = znajdzSilnie(v)
# print("Silnia liczby ", v, " jest równa: ", z)

def sprawdzParzystosc(x):
    if (x % 2 == 0):
        return "Parzysta"
        # print("Liczba ",x," jest parzysta")
    else:
        return "Nieparzysta"
        # print("Liczba ",x," nie jest parzysta")


# v=int(input("Podaj liczbę: "))
# sprawdzParzystosc(v)

def sprawdzParzystoscPrzedzialu(x, y):
    if (x < y):
        for i in range(x, y):
            print("Liczba ", i, " jest ", sprawdzParzystosc(i))


# sprawdzParzystoscPrzedzialu(3, 19)
